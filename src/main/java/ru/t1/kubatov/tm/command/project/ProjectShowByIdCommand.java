package ru.t1.kubatov.tm.command.project;

import ru.t1.kubatov.tm.model.Project;
import ru.t1.kubatov.tm.util.TerminalUtil;

public class ProjectShowByIdCommand extends AbstractProjectCommand {

    public final static String DESCRIPTION = "Show Project by ID.";

    public final static String NAME = "project-show-by-id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findById(getUserId(), id);
        showProject(project);
    }
}
