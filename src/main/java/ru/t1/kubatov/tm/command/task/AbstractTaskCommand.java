package ru.t1.kubatov.tm.command.task;

import ru.t1.kubatov.tm.api.service.IProjectTaskService;
import ru.t1.kubatov.tm.api.service.ITaskService;
import ru.t1.kubatov.tm.command.AbstractCommand;
import ru.t1.kubatov.tm.enumerated.Role;
import ru.t1.kubatov.tm.enumerated.Status;
import ru.t1.kubatov.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    protected void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

    protected void showTaskList(final List<Task> tasks) {
        int index = 1;
        for (final Task task : tasks) {
            System.out.printf("%s[%s]. %s \n", index, task.getId(), task);
            index++;
        }
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
