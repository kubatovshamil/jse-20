package ru.t1.kubatov.tm.command;

import ru.t1.kubatov.tm.api.model.ICommand;
import ru.t1.kubatov.tm.api.service.IAuthService;
import ru.t1.kubatov.tm.api.service.IPropertyService;
import ru.t1.kubatov.tm.api.service.IServiceLocator;
import ru.t1.kubatov.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    protected IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }

    public abstract Role[] getRoles();

    protected IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }


    public String getUserId() {
        return getAuthService().getUserId();
    }

}
