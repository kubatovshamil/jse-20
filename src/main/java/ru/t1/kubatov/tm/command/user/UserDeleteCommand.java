package ru.t1.kubatov.tm.command.user;

import ru.t1.kubatov.tm.enumerated.Role;
import ru.t1.kubatov.tm.util.TerminalUtil;

public class UserDeleteCommand extends AbstractUserCommand {

    public final static String DESCRIPTION = "Delete User.";

    public final static String NAME = "user-delete";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DELETE USER]");
        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();
        getUserService().deleteByLogin(login);
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }
}
