package ru.t1.kubatov.tm.command.task;

import ru.t1.kubatov.tm.util.TerminalUtil;

public class TaskCreateCommand extends AbstractTaskCommand {

    public final static String DESCRIPTION = "Create a Task.";

    public final static String NAME = "task-create";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        getTaskService().add(getUserId(), name, description);
    }
}
