package ru.t1.kubatov.tm.exception.system;

public class AccessDeniedException extends AbstractSystemException {

    public AccessDeniedException() {
        super("Error! Wrong Login or Password...");
    }

}
