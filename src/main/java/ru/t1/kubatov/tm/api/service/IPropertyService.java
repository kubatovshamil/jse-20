package ru.t1.kubatov.tm.api.service;

public interface IPropertyService extends ISaltProvider {

    String getApplicationVersion();

    String getAuthorEmail();

    String getAuthorName();

}
