package ru.t1.kubatov.tm.api.component;

public interface IBootstrap {

    void run(String... args);

}