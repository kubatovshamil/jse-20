package ru.t1.kubatov.tm.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.t1.kubatov.tm.api.service.ISaltProvider;

import java.security.MessageDigest;

public interface HashUtil {

    Logger LOGGER_LIFECYCLE = LoggerFactory.getLogger("LIFECYCLE");

    static String salt(final String value, final String secret, final Integer iteration) {
        if (value == null || secret == null || iteration == null) return null;
        String result = value;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

    static String salt(final ISaltProvider saltProvider, final String value) {
        if (saltProvider == null) return null;
        final String secret = saltProvider.getPasswordSecret();
        final Integer iteration = saltProvider.getPasswordIteration();
        return salt(value, secret, iteration);
    }

    static String md5(final String value) {
        if (value == null) return null;
        try {
            final MessageDigest md = MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(value.getBytes());
            final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; i++) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (final Exception e) {
            LOGGER_LIFECYCLE.error(e.getMessage());
            System.out.println("[FAIL]");
        }
        return null;
    }

}
