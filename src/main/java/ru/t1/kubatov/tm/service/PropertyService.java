package ru.t1.kubatov.tm.service;

import ru.t1.kubatov.tm.api.service.IPropertyService;

import java.io.IOException;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    public static final String FILE_NAME = "application.properties";

    public static final String APPLICATION_VERSION_KEY = "application.version";

    public static final String AUTHOR_EMAIL_KEY = "author.email";

    public static final String AUTHOR_NAME_KEY = "author.name";

    public static final String PASSWORD_ITERATION_DEFAULT = "100";

    public static final String PASSWORD_ITERATION_KEY = "password.iteration";

    public static final String PASSWORD_SECRET_DEFAULT = "123";

    public static final String PASSWORD_SECRET_KEY = "password.secret";

    public static final String EMPTY_VALUE = "--//--";

    private final Properties properties = new Properties();

    public PropertyService() {
        try {
            properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getApplicationVersion() {
        return getStringValue(APPLICATION_VERSION_KEY);
    }

    @Override
    public String getAuthorEmail() {
        return getStringValue(AUTHOR_EMAIL_KEY);
    }

    @Override
    public String getAuthorName() {
        return getStringValue(AUTHOR_NAME_KEY);
    }

    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    private String getStringValue(final String key, final String defaultValue) {
        return properties.getProperty(key, defaultValue);
    }

    private String getStringValue(final String key) {
        if (System.getProperties().contains(key)) return System.getProperties().getProperty(key);
        final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return getStringValue(key, EMPTY_VALUE);
    }

    private Integer getIntegerValue(final String key, final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    private String getEnvKey(final String key) {
        return key.replace(".", "_").toUpperCase();
    }

}
